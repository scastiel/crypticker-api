const express = require('express')
const cors = require('cors')
const morgan = require('morgan')
const logger = require('./logger')
const {
  fetchCurrencies,
  fetchAvailableTickers,
  fetchTickersValues,
  UnknownTickerError
} = require('./krakenApi')

const app = express()
app.use(cors())
app.use(morgan('tiny', { stream: logger.stream }))

function handleError(err, res) {
  logger.error(err.stack)
  res.status(500).end()
}

app.get('/currencies', (req, res) => {
  fetchCurrencies()
    .then(currencies => res.json(currencies))
    .catch(err => handleError(err, res))
})

app.get('/availableTickers', (req, res) => {
  fetchAvailableTickers()
    .then(tickers => res.json(tickers))
    .catch(err => handleError(err, res))
})

app.get('/tickersValues', (req, res) => {
  const tickers = req.query.tickers && req.query.tickers.split(',')
  if (tickers) {
    fetchTickersValues(tickers)
      .then(tickersValues => res.json(tickersValues))
      .catch(err => {
        if (err instanceof UnknownTickerError) {
          logger.warn(err.stack)
          res.status(400).end(err.message)
        } else {
          handleError(err, res)
        }
      })
  } else {
    res.json([])
  }
})

const port = process.env.PORT || 3001
app.listen(port, () => {
  logger.info(`Listening on port ${port}`)
})
