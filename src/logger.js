const winston = require('winston')

const logger = new winston.Logger({
  transports: [
    new winston.transports.Console({ level: 'debug', colorize: true })
  ]
})

logger.stream = {
  write: function(message, encoding) {
    logger.info(message.replace(/\n$/, ''))
  }
}

module.exports = logger
