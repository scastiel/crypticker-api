const fetch = require('isomorphic-fetch')
const logger = require('./logger')

class UnknownTickerError extends Error {}

function fetchCurrencies() {
  logger.debug('Fetching currencies…')
  return fetch('https://api.kraken.com/0/public/Assets', {
    mode: 'cors'
  })
    .then(res => res.json())
    .then(res => {
      if (res.error && res.error.length > 0) {
        throw new Error(res.error[0])
      }
      const assetsIds = Object.keys(res.result)
      logger.debug(`Fetched ${assetsIds.length} currencies.`)
      return assetsIds.map(assetId => ({
        id: assetId,
        symbol: res.result[assetId].altname === 'XBT'
          ? 'BTC'
          : res.result[assetId].altname
      }))
    })
}

function fetchAvailableTickers() {
  logger.debug('Fetching available tickers…')
  return fetch('https://api.kraken.com/0/public/AssetPairs', { mode: 'cors' })
    .then(res => res.json())
    .then(res => {
      if (res.error && res.error.length > 0) {
        throw new Error(res.error[0])
      }
      const tickersIds = Object.keys(res.result)
      logger.debug(`Fetched ${tickersIds.length} available tickers.`)
      return tickersIds
        .filter(tickerId => !tickerId.match(/\.d$/))
        .map(tickerId => ({
          id: tickerId,
          currFrom: res.result[tickerId].base === 'XBT'
            ? 'BTC'
            : res.result[tickerId].base,
          currTo: res.result[tickerId].quote === 'XBT'
            ? 'BTC'
            : res.result[tickerId].quote,
          value: null
        }))
    })
}

function fetchTickersValues(tickers) {
  logger.debug('Fetching tickers values for tickers:', tickers)
  return fetch(
    'https://api.kraken.com/0/public/Ticker?pair=' + tickers.join(',')
  )
    .then(res => res.json())
    .then(res => {
      if (res.error && res.error.length > 0) {
        if (res.error[0].match(/Unknown asset pair/)) {
          throw new UnknownTickerError(
            `Wrong ticker given as parameter: ${tickers.join(', ')}`
          )
        } else {
          throw new Error(res.error[0])
        }
      }
      logger.debug(`Fetched ${tickers.length} tickers values.`)
      return tickers.map(tickerId => {
        const id = tickerId
        const value = parseFloat(res.result[tickerId].c[0])
        return { id, value }
      })
    })
}

module.exports = {
  fetchCurrencies,
  fetchAvailableTickers,
  fetchTickersValues,
  UnknownTickerError
}
